---
name: Job Fair
---

Our annual job fair during DebConf connects our qualifying sponsors with
DebConf participants interested in new professional opportunities.

The Job Fair will be held in the open spaces of the 1st (ground) floor
of the MIRC building.
