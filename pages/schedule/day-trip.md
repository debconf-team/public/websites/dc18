---
name: Day Trip
---
This is the day for socialization, for spending some hours away from
your screen, and get to know a little bit of Taiwan!

Please make sure to read the [wiki page][] and sign up for one of the
options.

[wiki page]: https://wiki.debconf.org/wiki/DebConf18/DayTrip
