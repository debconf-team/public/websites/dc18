---
name: Coffee Break
---
Not a meal per-se, but Debianites need their fuel.

Served from 16:00–16:30 daily, on the ground floor of the MIRC.
