---
name: Conference Dinner
---
Every year, we celebrate DebConf with a special dinner.

It will be at Jimei Seafood Restaurant 佳美海鮮餐廳 at 18:30–21:45 on
August 2.

Buses will take attendees to the dinner at 17:45 from outside
Dormitory 13 (to the left of Dormitory 12, where there Family Mart is).

The buses will return to NCTU at 21:45.
If you miss the last bus, you will need to find your own way back.

There will be some brief speeches from:

1. NCTU / UST Vice Chancellor [Jason Yi-Bing Lin][en] [林一平副校長][cn]

[en]: http://www.nctu.edu.tw/vc3-member-en
[cn]: http://www.nctu.edu.tw/administration/vicepresidentoffice3/vicepresident-member

Dinner Venue Address:

* No. 715, Section 2, Yanping Road, Xiangshan District, Hsinchu City, 300
* 300新竹市香山區延平路二段715號
