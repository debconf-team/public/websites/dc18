---
title: DebConf18 dates announced at DebConf17
---

Next year, DebConf will be held in Hsinchu, Taiwan, from 29 July to 5
August 2018. For the days before DebConf the local organisers will again set up
DebCamp, a session for some intense work on improving the distribution, from
July 21 to 27, and the Debian Day on July 28, aimed at the general public.

Debian thanks the commitment of the
[sponsors](https://debconf18.debconf.org/sponsors/) supporting DebConf18.
