# -*- coding: utf-8 -*-
from django.core.mail import EmailMultiAlternatives
from django.core.management.base import BaseCommand

from register.models.attendee import Attendee


SUBJECT = u'[DebConf 18]: Willing to share your full name with government sponsors?'
TXT = u"""Hi,

Are you willing to share your *full* name and nationality with Taiwanese
government sponsors?

If so, please do so at: https://debconf18.debconf.org/attendees/list_opt_in/


The Background:

DebConf18 received some sponsorship from the Taiwanese government, through the
National Chiao Tung University (NCTU), the National Centre of High-performance
Computing (NCHC), and the MEET TAIWAN program.

Some of this funding requires these organisations to show that our conference
was attended by a sufficiently international audience. For this, they need a
list of *full names* and nationalities [1]. You don't have to opt-in to this,
but we do need at least 100 people to do so, in order to receive this funding.

As we didn't provide this opt-in during registration, and we didn't state that
we'd be sharing this data, we're asking you to consider opting-in now.

We previously thought we could just share initials, with an opt-out process.
As that turns out not to be enough, full names are required. Thus this opt-in.


Thanks for attending, we hope you enjoyed DebConf18 as much as we did.

Please send any feedback (positive and negative) to <feedback@debconf.org>, so
we can make DebConf even better, next year.


The DebConf registration team

[1]: Yes, there has been negotiation on this. Statistics are insufficient, and
     full names are required.
     Previously, they wanted more data, but we've negotiated down to just names
     and nationalities.
"""


class Command(BaseCommand):
    help = (
        'Send an email asking people to opt into sharing data with government '
        'sponsors')

    def add_arguments(self, parser):
        parser.add_argument('--yes', action='store_true',
                            help='Actually do something')

    def badger(self, attendee, dry_run):
        if dry_run:
            user_profile = attendee.user.userprofile
            print('I would badger: %s'
                  % user_profile.display_name().encode('utf-8'))
            return
        to = attendee.user.email
        email_message = EmailMultiAlternatives(SUBJECT, TXT, to=[to])
        email_message.send()

    def handle(self, *args, **options):
        dry_run = not options['yes']
        if dry_run:
            print('Not actually doing anything without --yes')

        for attendee in Attendee.objects.filter(check_in__pk__isnull=False):
            self.badger(attendee, dry_run)
